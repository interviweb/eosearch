# EOSearch

Simple and lightweight searcher in objects. Works in browser and Node.

## Install

`npm i eosearch`

## API

* **EOSearch.addIndex(arr)** - add aray keys to search in
    * **arr** example - `[name, description]`
* **EOSearch.addDocument(obj)** - add object to search in
    * **obj** example - `{name: 'test', description: 'i doc'}`
* **EOSearch.addDocuments(arr)** - add multiple objects
    * **arr** example - `[{name: 'a', description: 'b'}, {name: 'c', description: 'd'}]`
* **EOSearch.search(query, params)** - search
    * **query** - regex object or string
    * **params** - `{regex: true, nocase: false}`
        * **regex** - boolean, if true - query is regular expression
        * **nocase** - boolean, if true - query and fields to lower before search

**search** return array:

```
[
    {
        item: you object,
        data: [
            field: text field,
            index: start index,
            keys: [] //keys hierarchy
        ]
    }
]
```

## Example

from *test.js*

```
const EOSearch = require('eosearch');

let search = new EOSearch();
search.addIndex(['text']);
search.addDocument({
    text: 'test abcd'
});
let result = search.search('es');
console.log(JSON.stringify(result));
```

will be print to console

`[{"item":{"text":"test abcd"},"data":[{"field":"test abcd","index":1,"keys":["text"]}]}]`
