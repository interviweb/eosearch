class EOSearch {
    constructor() {
        this._index = [];
        this._docs = [];
    }
    
    /**
     * Add keys hierarchy to search in.
     * @param {Array<string>} arr
     */
    addIndex(arr) {
        if (!Array.isArray(arr)) {
            throw new TypeError('arr is not Array');
        }
        this._index.push(arr);
    }
    
    /**
     * Add documents to search in.
     * @param {Object} obj
     */
    addDocument(obj) {
        if (typeof(obj) !== 'object') {
            throw new TypeError('obj is not Object');
        }
        this._docs.push(obj);
    }
    
    /**
     * Add multiple documents to search in.
     * @param {Array<Object>} arr
     */
    addDocuments(arr) {
        if (!Array.isArray(arr)) {
            throw new TypeError('arr is not Array');
        }
        for (let i = 0; i < arr.length; i++) {
            this.addDocument(arr[i]);
        }
    }
    
    /**
     * Search in all documents.
     * @param {String/Object} query - object if use regex or string
     * @param {Object} params - nocase - query and field to lower, regex - use regex
     * @returns {Array<Object>} - example (see test.js): [{"item":{"text":"test abcd"},"data":[{"field":"test abcd","index":1,"keys":["text"]}]}]
     */
    search(query, params) {
        if (params && typeof(params) !== 'object') {
            throw new TypeError('params is not Object');
        }
        if (!params) params = {}
        if (params.regex) {
            if (typeof(query) !== 'object') {
                throw new TypeError('query is not regex Object');
            }
        } else {
            if (typeof(query) !== 'string') {
                throw new TypeError('query is not string');
            }
            if (params.nocase) {
                query = query.toLowerCase();
            }
        }
        let result = [];
        for (let i = 0; i < this._docs.length; i++) {
            let item = this._docs[i];
            let fields = [];
            for (let n = 0; n < this._index.length; n++) {
                let keys = this._index[n];
                let field = Object.assign({}, item);
                for (let k = 0; k < keys.length; k++) {
                    let key = keys[k];
                    if (Object.prototype.hasOwnProperty.call(field, key)) {
                        field = field[key];
                    } else {
                        field = null;
                        break;
                    }
                }
                if (typeof(field) !== 'string') {
                    continue;
                }
                let index = -1;
                if (params.regex) {
                    index = field.search(query);
                } else {
                    if (params.nocase) {
                        index = field.toLowerCase().indexOf(query);
                    } else {
                        index = field.indexOf(query);
                    }
                }
                if (index === -1) continue;
                fields.push({
                    field: field,
                    index: index,
                    keys: keys
                })
            }
            if (!fields.length) continue;
            result.push({
                item: Object.assign({}, item),
                data: fields
            });
        }
        return result;
    }
}

if (module && module.exports) {
    module.exports = EOSearch;
}
