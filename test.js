const EOSearch = require('./index');

let search = new EOSearch();
search.addIndex(['text']);
search.addDocument({
    text: 'test abcd'
});
let result = search.search('es');
console.log(JSON.stringify(result));
console.assert(result);
